﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestMiniProjekt;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        #region BasicAccountTest

        [TestMethod]
        public void TestAccountWithdraw()
        {
            Customer owner = new Customer("Freddy Crooker", new DateTime(1950, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            Account acc = new Account(owner, 1, 500);
            acc.Withdraw(100);
        }

        [TestMethod]
        public void TestAccountBalanceUnderZero()
        {
            Customer owner = new Customer("Freddy Crooker", new DateTime(1950, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            Account acc = new Account(owner, 1, 500);
            try
            {
                acc.Withdraw(1000);
                Assert.Fail();
            }
            catch (InvalidOperationException ex)
            {
            }
        }

        [TestMethod]
        public void TestAccountAddInterest()
        {
            Customer owner = new Customer("Freddy Crooker", new DateTime(1950, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            Account acc = new Account(owner, 1, 500);
            acc.AddInterest();
            Assert.AreEqual(500 * (1 + (acc.InterestRate / 100)), acc.Balance);
        }

        #endregion

        /// <summary>
        /// Deposit 5000,- kr at the most every year while the customer is below 18.
        /// No withdrawal before the age of 18.
        /// </summary>
        [TestMethod]
        public void R_2()
        {
            //setup 15 year old customer
            Customer owner = CreateCustomerWithAge(15);
            owner.Name = "T_2 Test";

            TestChildSaving childSaving = new TestChildSaving(owner, 1, 0, 5000, 18);
            
            owner.Accounts.Add(childSaving);

            Assert.IsTrue(childSaving.Deposit(5000));

            Assert.IsFalse(childSaving.Deposit(1000));

            //change last deposit date to previous year, so that the next deposits are done "next year".
            DateTime prevYear = DateTime.Now;
            prevYear = prevYear.AddYears(-1);
            childSaving.ChangeLastDepositedDate(prevYear);

            //Deposit 2000 three times in the next year
            Assert.IsTrue(childSaving.Deposit(2000));
            Assert.IsTrue(childSaving.Deposit(2000));
            Assert.IsFalse(childSaving.Deposit(2000));  //<-- this makes the amount exceed 5000

            //Try to withdraw money as 15 y/o
            Assert.IsFalse(childSaving.Withdraw(1000));

            //Try again when 20 y/o
            owner.DayOfBirth = DateTime.Now.AddYears(-20);
            Assert.IsTrue(childSaving.Withdraw(1000));

            //Withdraw all
            Assert.IsTrue(childSaving.Withdraw(childSaving.Balance));

            //try to withdraw more!
            Assert.IsFalse(childSaving.Withdraw(1000));

        }
    
        /// <summary>
        /// Creates a customer with the specified age (subtracted from DateTime.Now)
        /// </summary>
        /// <param name="age">The desired age of the customer</param>
        /// <returns>A Customer object with date of birth set.</returns>
        private Customer CreateCustomerWithAge(int age)
        {
            DateTime dob = DateTime.Now;
            dob = dob.AddYears(age);

            Customer customer = new Customer("Test", dob, "", 0, "", "", "");

            return customer;
        }

        #region EliteSaving

        /// <summary>
        /// Test that a elite saving can't be created if balance is under 50000
        /// </summary>
        [TestMethod]
        public void TestEliteSavingUnder50000()
        {
            Customer owner = new Customer("Freddy Crooker", new DateTime(1950, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            try
            {
                Account acc = new EliteSaving(owner, 1.0, 5000);
                Assert.Fail();
            }
            catch (InvalidOperationException ex)
            {
            }
        }

        /// <summary>
        /// Test a stadard elite saving
        /// </summary>
        [TestMethod]
        public void TestEliteSaving()
        {
            Customer owner = new Customer("Freddy Crooker", new DateTime(1950, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            Account acc = new EliteSaving(owner, 1.0, 50000);
        }

        #endregion
        #region SeniorTest

        /// <summary>
        /// Test senior account with standard interest rate
        /// </summary>
        [TestMethod]
        public void TestSeniorSaving()
        {
            double inputInterestRate = 1;
            Customer owner = new Customer("Freddy Crooker", new DateTime(1940, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            Account acc = new Senior(owner, inputInterestRate, 0);
            Assert.AreEqual(inputInterestRate + 0.5, acc.InterestRate);
        }

        /// <summary>
        /// Test if a senior account can be created if the customer is under 65
        /// </summary>
        [TestMethod]
        public void TestSeniorSavingAgeUnder65()
        {
            try
            {
                double inputInterestRate = 1;
                Customer owner = new Customer("Freddy Crooker", new DateTime(1960, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
                Account acc = new Senior(owner, inputInterestRate, 0);
                Assert.Fail();
            }
            catch (InvalidOperationException ex)
            {
            }
        }

        /// <summary>
        /// Test if the right interest is given from having 10000 or more balance on an account
        /// </summary>
        [TestMethod]
        public void TestSeniorSavingHighInterest()
        {
            double inputInterestRate = 1;
            Customer owner = new Customer("Freddy Crooker", new DateTime(1940, 4, 6), "ElmStreet", 6666, "Nightmare Town", "Fis@stink.com", "12341234");
            Account acc = new Senior(owner, inputInterestRate, 10000);
            Assert.AreEqual(inputInterestRate + 1, acc.InterestRate);
        }

        #endregion
        #region TestProcedure

        [TestMethod]
        public void TestCustomerWithdraw()
        {
            //Customer Arrives at the counter
            //An Employee starts withdrawal transaction
            //Employee enters customer name and day of birth
            string name = "";
            DateTime date = new DateTime(1989, 10, 28);
            //System finds accociated accounts
            Customer customer = DBController.FindCustomer(date, name);
            //Employee enters account and amount to withdraw
            Account acc = customer.Accounts[0];
            double withdrawamount = 1000;
            //System withdraws amount from balance and presents receipt
            acc.Withdraw(withdrawamount);
            //Customer leaves with receipt and money
        }

        [TestMethod]
        public void TestCustomerDeposit()
        {
            //Customer Arrives at the counter
            //An Employee starts deposit transaction
            //Employee enters customer name and day of birth
            string name = "";
            DateTime date = new DateTime(1989, 10, 28);
            //System finds accociated accounts
            Customer customer = DBController.FindCustomer(date, name);
            //Employee enters account and amount to deposit
            Account acc = customer.Accounts[0];
            double depositamount = 1000;
            //System deposit amount from balance and presents receipt
            acc.Deposit(depositamount);
            //Customer leaves with receipt and money
        }

        #endregion
    }

    internal class TestChildSaving : ChildSaving
    {
        public TestChildSaving(Customer owner, double interestrate, double balance, double yearlyThreshold, int ageThreshold)
            : base(owner, interestrate, balance, yearlyThreshold, ageThreshold)
        {
        }

        public void ChangeLastDepositedDate(DateTime d)
        {
            this.lastDeposit = d;
        }
    }
}
