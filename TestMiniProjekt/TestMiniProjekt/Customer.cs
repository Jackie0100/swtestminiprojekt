﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public class Customer
    {
        public string Name { get; set; }
        public DateTime DayOfBirth { get; set; }
        public string Street { get; set; }
        public ushort ZipCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public List<Account> Accounts { get; set; }

        public Customer(string name, DateTime dayofbirth, string street, ushort zipcode, string city, string email, string phoneno)
        {
            Accounts = new List<Account>();
            Name = name;
            DayOfBirth = dayofbirth;
            Street = street;
            ZipCode = zipcode;
            City = city;
            Email = email;
            PhoneNo = phoneno;
        }

        public int GetAge()
        {
            int yearBorn = DayOfBirth.Year;
            int yearNow = DateTime.Now.Year;

            int age = yearNow - yearBorn;

            //if owner hasn't had birthday yet
            if (DayOfBirth.DayOfYear < DateTime.Now.DayOfYear)
            {
                age -= 1;
            }

            return age;
        }
    }
}
