﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public class Senior : Account
    {
        public override double InterestRate
        {
            get
            {
                if (Balance >= 10000)
                {
                    return base.InterestRate + 1.0;
                }
                else
                {
                    return base.InterestRate + 0.5;
                }
            }
            set
            {
                base.InterestRate = value;
            }
        }

        public Senior(Customer owner, double interestrate, double balance)
            : base(owner, interestrate, balance)
        {
            if (owner.DayOfBirth.AddYears(65) > DateTime.Now)
            {
                throw new InvalidOperationException("a owner of a Senior account must be 65+ years old");
            }
        }
    }
}
