﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public static class ExtentionClass
    {
        public static ulong Get64BitRandom(this Random rnd, ulong minValue, ulong maxValue)
        {
            // Get a random array of 8 bytes. 
            // As an option, you could also use the cryptography namespace stuff to generate a random byte[8]
            byte[] buffer = new byte[sizeof(ulong)];
            rnd.NextBytes(buffer);
            return BitConverter.ToUInt64(buffer, 0) % (maxValue - minValue + 1) + minValue;
        }
    }
}
