﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public class ChildSaving : Account
    {
        //when the last deposit was made
        protected DateTime lastDeposit;

        //amount deposited this calendar year
        private double depositedThisYear;

        //how much can be deposited in one calendar year
        private double yearlyThreshold;
        
        //how old must owner be to withdraw money?
        private int ageThreshold;

        public ChildSaving(Customer owner, double interestrate, double balance, double yearlyThreshold, int ageThreshold)
            : base(owner, interestrate, balance)
        {
            this.yearlyThreshold = yearlyThreshold;
            this.ageThreshold = ageThreshold;

            lastDeposit = DateTime.Now;
            lastDeposit.Subtract(new TimeSpan(700,0,0,0));

            depositedThisYear = 0;
        }

        public override bool Deposit(double amount)
        {
            bool retVal = false;

            //was the last deposit made last year?
            if (lastDeposit.Year < DateTime.Now.Year)
            {
                depositedThisYear = 0;
            }

            //is amount within the yearly threshold?
            if (depositedThisYear + amount <= yearlyThreshold)
            {
                Balance += amount;
                depositedThisYear += amount;
                lastDeposit = DateTime.Now;
                retVal = true;
            }

            return retVal;
        }

        public override bool Withdraw(double amount)
        {
            bool retVal = false;

            //owner must be a specific age before being able to withdraw
            int age = Owner.GetAge();
            if (age >= ageThreshold)
            {
                //one cannot withdraw more than balance
                if (amount <= Balance)
                {
                    retVal = true;
                    Balance -= amount;
                }
                else
                {
                    //TODO: throw AmountException()
                }
            }
            else
            {
                //TODO: throw AgeException()
            }

            return retVal;
        }
    }
}
