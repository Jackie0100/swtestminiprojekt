﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public static class DBController
    {
        public static List<Customer> Customers;
        public static List<Account> Accounts = new List<Account>();

        static DBController()
        {
            Customers =  new List<Customer>() { 
            new Customer("Silje V. Mathiasen", new DateTime(1987,2,14),"Mellemvej 40", 9440, "Aabybro","SiljeVMathiasen@jourrapide.com", "30198757"),
            new Customer("Sandra S. Frederiksen", new DateTime(1979,1,27),"Søndervænget 59", 1156 , "København K","SandraSFrederiksen@dayrep.com", "20737962"),
            new Customer("Maria A. Mathiesen", new DateTime(1982,9,16),"Tarupvej 20", 2770, "Kastrup","MariaAMathiesen@armyspy.com", "42773357"),
            new Customer("Magnus O. Berg",new DateTime(1923,10,12),"Skibbroen 71",5320,"Agedrup","MagnusOBerg@teleworm.us","30573623"),
            new Customer("Jakob N. Olsen",new DateTime(1962,1,5),"Sønderstræde 56",1706,"København V","JakobNOlsen@gustr.com","21887237"),
            new Customer("Mike S. Bruun",new DateTime(1930,8,1),"Ladbyvej 78",5450,"Otterup","MikeSBruun@superrito.com","22-75-42-34"),
            new Customer("Alexander R. Nygaard",new DateTime(1973,12,15),"Grønlandsgade 38",1783,"København V","AlexanderRNygaard@jourrapide.com","41872796"),
            new Customer("Pernille D. Skov",new DateTime(1981,7,10),"Sveltekrogen 65",1561,"København V","PernilleDSkov@gustr.com","71360239"),
            new Customer("Mathilde J. Lauridsen",new DateTime(1933,3,12),"Højbovej 5",1115,"København K","MathildeJLauridsen@dayrep.com","41405375"),
            new Customer("Mie A. Christiansen",new DateTime(1943,3,29),"Gammelhavn 73",8592,"Anholt","MieAChristiansen@armyspy.com","21294834"),
            new Customer("Esther M. Lorenzen",new DateTime(1996,3,15),"Bygmestervej 99",1601,"København V","EstherMLorenzen@dayrep.com","40926747"),
            new Customer("Anne N. Lauritse",new DateTime(1930,7,19),"Eskelundsvej 32",1551,"København V","AnneNLauritsen@cuvox.de","26700898"),
            new Customer("Jacob M. Kjeldsen",new DateTime(1953,4,14),"Blæsenborgvej 6",1054,"København K","JacobMKjeldsen@cuvox.de","27519505"),
            new Customer("Jakob K. Nissen",new DateTime(1989,10,28),"Torpegårdsvej 93",4490,"Jerslev Sjælland","JakobKNissen@armyspy.com","28271273"),
            new Customer("Jeppe K. Dam",new DateTime(1925,4,23),"Sveltekrogen 90",1562,"København V","JeppeKDam@teleworm.us","21561623"),
            new Customer("Caroline L. Bang",new DateTime(1994,1,12),"Gartnervænget 82",6623,"Vorbasse","CarolineLBang@armyspy.com","40615408"),
            new Customer("Martin J. Winther",new DateTime(1992, 11, 28),"Fynshovedvej 41",2670,"Greve","MartinJWinther@jourrapide.com","30749126"),
            new Customer("Jens R. stergaard",new DateTime(1947,12,30),"Ørbækvej 44",1809,"Frederiksberg C","JensRstergaard@rhyta.com","42320789"),
            new Customer("Rolla B. Nrgaard",new DateTime(2000, 7, 29),"Nørremarksvej 12",1608,"København V","RollaBNrgaard@cuvox.de","53967020"),
            new Customer("Astrid S. Nissen", new DateTime(2011,4,7), "Østervoldgade 98",1957,"Frederiksberg C","AstridSNissen@rhyta.com","50202851"),
            };

            Random rnd = new Random(1234);

            for (int i = 0; i < Customers.Count; i++)
            {
                if (Customers[i].GetAge() < 12)
                {
                    Accounts.Add(new ChildSaving(Customers[i], (rnd.Next(5, 20) / 10.0), rnd.Next(5000, 50000), 5000, 18));
                }
                if (Customers[i].GetAge() < 15)
                {
                    Accounts.Add(new Youngster(Customers[i], (rnd.Next(5, 20) / 10.0), rnd.Next(5000, 10000)));
                }
                if (Customers[i].GetAge() >= 20 && Customers[i].GetAge() < 65)
                {
                    Accounts.Add(new Account(Customers[i], ((double)rnd.Next(5, 10) / 10.0), rnd.Next(5000, 50000)));
                }
                if (Customers[i].GetAge() >= 65)
                {
                    Accounts.Add(new Senior(Customers[i], ((double)rnd.Next(5, 10) / 10.0), rnd.Next(5000, 50000)));
                }
                if (rnd.Next(0, 5) == 0)
                {
                    Accounts.Add(new EliteSaving(Customers[i], ((double)rnd.Next(5, 25) / 10.0), rnd.Next(50000, 250000)));
                }
            }
        }

        public static Customer FindCustomer(DateTime birthday, string name = "")
        {
            return Customers.First(c => c.DayOfBirth.Year == birthday.Year && c.DayOfBirth.Month == birthday.Month && c.DayOfBirth.Day == birthday.Day && c.Name.Contains(name));
        }
    }
}
