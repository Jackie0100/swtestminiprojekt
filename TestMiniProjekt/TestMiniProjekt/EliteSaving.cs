﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public class EliteSaving : Account
    {
        public override double Balance
        {
            get
            {
                return base.Balance;
            }
            set
            {
                if (value < 50000)
                {
                    throw new InvalidOperationException("the balance of an elite saving can't be under 50000");
                }
                base.Balance = value;
            }
        }

        public override double InterestRate
        {
            get
            {
                if (Balance >= 50000)
                {
                    return base.InterestRate + 2.0;
                }
                else
                {
                    return base.Balance;
                }
            }
            set
            {
                base.InterestRate = value;
            }
        }

        public EliteSaving(Customer owner, double interestrate, double balance) : base (owner, interestrate, balance)
        {
            Balance = balance;
        }
    }
}
