﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TestMiniProjekt
{
    public class Account
    {
        private double balance;
        private double interestRate;

        public ulong AccountNo { get; set; }

        public virtual double Balance
        {
            get
            {
                return balance;
            }
            set
            {
                if (value < 0)
                {
                    throw new InvalidOperationException("Balance of an account can't be below 0");
                }
                balance = value;
            }
        }

        public virtual double InterestRate
        {
            get
            {
                return interestRate;
            }
            set
            {
                if (value < 0)
                {
                    throw new InvalidOperationException("Interest Rate of an account can't be below 0");
                }
                interestRate = value;
            }
        }

        public Customer Owner { get; private set; }


        public Account(Customer owner, double interestrate, double balance)
        {
            Owner = owner;
            Random rnd = new Random(Owner.Name.GetHashCode());
            AccountNo = rnd.Get64BitRandom(1, 9999999999);
            while (DBController.Accounts.Exists(a => a.AccountNo == AccountNo))
            {
                AccountNo = rnd.Get64BitRandom(1, 9999999999);
            }
            InterestRate = interestrate;
            Balance = balance;
            Owner.Accounts.Add(this);
        }

        public virtual bool Deposit(double amount)
        {
            Balance += amount;
            return true;
        }

        public virtual bool Withdraw(double amount)
        {
            Balance -= amount;
            return true;
        }

        public virtual void AddInterest()
        {
            Balance *= (1 + (InterestRate / 100));
        }
    }
}
