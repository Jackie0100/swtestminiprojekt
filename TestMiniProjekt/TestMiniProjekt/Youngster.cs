﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestMiniProjekt
{
    public class Youngster : Account
    {
        private double extrainterest = 1.5;

        public override double InterestRate
        {
            get
            {
                if (Owner.DayOfBirth.AddYears(20) > DateTime.Now && Balance < 25000)
                {
                    return base.InterestRate + extrainterest;
                }
                else
                {
                    return base.InterestRate;
                }
            }
            set
            {
                base.InterestRate = value;
            }
        }

        public Youngster(Customer owner, double interestrate, double balance) : base(owner, interestrate, balance)
        {
            //currentDate is for debugging purposes.
            DateTime currentDate = DateTime.Now.AddYears(-20);

            if (owner.DayOfBirth < currentDate)
            {
                throw new Exception("A Youngster account can only be created for people below 20 years.");
            }
            Balance = balance;
        }

        public override void AddInterest()
        {
            if (Balance > 25000)
            {
                double tempinterest = (25000 * (1 + ((InterestRate + extrainterest) / 100))) - 25000;
                tempinterest += ((Balance - 25000) * (1 + (InterestRate / 100))) - (Balance - 25000);
                Balance += tempinterest;
            }
            else
            {
                base.AddInterest();
            }
        }
    }
}
